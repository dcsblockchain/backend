package com.slalom.tchainz.web.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TitleTransferData {
    private Long buyerId; //license number
    private String buyerFirstName;
    private String buyerLastName;
    private String sellerFirstName;
    private String sellerLastName;
    private String address;
    private int zipCode;
    private String county;
    private Date saleContractDate;
    private String telephoneNumber;
    private BigDecimal salesPrice;

    private boolean isMobileHome;
    private String make;
    private String model;
    private String bodyType;
    private String color;
    private int year;

    public Long getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Long buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerFirstName() {
        return buyerFirstName;
    }

    public void setBuyerFirstName(String buyerFirstName) {
        this.buyerFirstName = buyerFirstName;
    }

    public String getBuyerLastName() {
        return buyerLastName;
    }

    public void setBuyerLastName(String buyerLastName) {
        this.buyerLastName = buyerLastName;
    }

    public String getSellerFirstName() {
        return sellerFirstName;
    }

    public void setSellerFirstName(String sellerFirstName) {
        this.sellerFirstName = sellerFirstName;
    }

    public String getSellerLastName() {
        return sellerLastName;
    }

    public void setSellerLastName(String sellerLastName) {
        this.sellerLastName = sellerLastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Date getSaleContractDate() {
        return saleContractDate;
    }

    public void setSaleContractDate(Date saleContractDate) {
        this.saleContractDate = saleContractDate;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public BigDecimal getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(BigDecimal salesPrice) {
        this.salesPrice = salesPrice;
    }

    public boolean isMobileHome() {
        return isMobileHome;
    }

    public void setMobileHome(boolean mobileHome) {
        isMobileHome = mobileHome;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return Long.toString(buyerId) +
            buyerFirstName +
            buyerLastName +
            sellerFirstName +
            sellerLastName +
            address +
            Integer.toString(zipCode) +
            county +
            (saleContractDate != null ? saleContractDate.toString() : "") +
            telephoneNumber +
            (salesPrice != null ? salesPrice.toString() : "" ) +
            Boolean.toString(isMobileHome) +
            make +
            model +
            bodyType +
            color +
            Integer.toString(year);
    }
}
