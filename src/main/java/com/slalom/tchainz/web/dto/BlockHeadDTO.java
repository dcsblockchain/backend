package com.slalom.tchainz.web.dto;

import java.io.Serializable;

public class BlockHeadDTO implements Serializable {
    private long blockSize;
    private Block head;

    public long getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(long blockSize) {
        this.blockSize = blockSize;
    }

    public Block getHead() {
        return head;
    }

    public void setHead(Block head) {
        this.head = head;
    }
}
