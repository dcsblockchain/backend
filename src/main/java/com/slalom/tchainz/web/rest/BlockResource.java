package com.slalom.tchainz.web.rest;

import com.slalom.tchainz.web.dto.BlockHeadDTO;
import com.slalom.tchainz.web.dto.TitleTransferData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BlockResource {
    private final Logger log = LoggerFactory.getLogger(BlockResource.class);

    private final DiscoveryClient discoveryClient;
    private final LoadBalancerClient loadBalancerClient;

    public BlockResource(DiscoveryClient discoveryClient,
                         LoadBalancerClient loadBalancerClient) {
        this.discoveryClient = discoveryClient;
        this.loadBalancerClient = loadBalancerClient;
    }

    @PostMapping("/blockchain/insertnewtitletransfer")
    public ResponseEntity<Void> insertNewTransaction(@RequestBody TitleTransferData transaction) {
        log.debug("Processing request to add a new transaction to the blockchain");

        RestTemplate restTemplate = new RestTemplate();

        List<ServiceInstance> nodeInstances = discoveryClient.getInstances("node");

        // return not found if there are no nodes online
        if (nodeInstances.size() == 0) {
            return ResponseEntity.notFound().build();
        }

        LoadBalancerRequest<Void> loadBalancerRequest = instance -> {
            System.out.println(instance.toString());
            restTemplate.postForEntity(instance.getUri().toString() + "/block", transaction, Void.class);
            return null;
        };

        try {
            loadBalancerClient.execute("node", loadBalancerRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("/blockchain/getchainfromindex/{index}")
    public ResponseEntity<List> getChainFromIndex(@PathVariable long index) {
        log.debug("Processing request to get the blocks from index {}", index);
        RestTemplate restTemplate = new RestTemplate();

        List titleList = new ArrayList();

        List<ServiceInstance> nodeInstances = discoveryClient.getInstances("node");

        // return not found if there are no nodes online
        if (nodeInstances.size() == 0) {
            return ResponseEntity.notFound().build();
        }

        LoadBalancerRequest<ResponseEntity<List>> loadBalancerRequest = instance -> restTemplate.getForEntity(instance.getUri().toString() + "/block/get-from/" + index, List.class);

        try {
            titleList = loadBalancerClient.execute("node", loadBalancerRequest).getBody();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok(titleList);
    }

    @GetMapping("/blockchain/getorigin")
    public ResponseEntity<BlockHeadDTO> getChainFromIndex() {
        log.debug("Processing request to get the blockchain head");
        RestTemplate restTemplate = new RestTemplate();

        BlockHeadDTO origin = null;

        List<ServiceInstance> nodeInstances = discoveryClient.getInstances("node");

        // return not found if there are no nodes online
        if (nodeInstances.size() == 0) {
            return ResponseEntity.notFound().build();
        }

        LoadBalancerRequest<ResponseEntity<BlockHeadDTO>> loadBalancerRequest = instance -> {
            System.out.println(instance.getUri());
            return restTemplate.getForEntity(instance.getUri().toString() + "/block/head", BlockHeadDTO.class);
        };

        try {
            origin =  loadBalancerClient.execute("node", loadBalancerRequest).getBody();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok(origin);

    }

    @GetMapping("/blockchain/search")
    public ResponseEntity<List> search(@RequestParam String tokens) {
        String[] tokenList = tokens.split(" ");
        System.out.println(tokenList);
        return ResponseEntity.ok(new ArrayList());
    }
}
