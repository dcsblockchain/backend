package com.slalom.tchainz.repository.dynamo;

import com.slalom.tchainz.domain.Authority;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Authority entity.
 */
@EnableScan
@Repository
public interface AuthorityRepository extends CrudRepository<Authority, String> {
}
