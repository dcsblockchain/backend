package com.slalom.tchainz.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.github.dynamobee.Dynamobee;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ApplicationProperties.class)
@EnableDynamoDBRepositories(basePackages = "com.slalom.tchainz.repository.dynamo")
public class DynamoDbConfig {

    @Autowired
    private ApplicationProperties applicationProperties;

    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        String endPoint = applicationProperties.getAmazon().getDynamoDb().getEndPoint();
        String signingRegion = applicationProperties.getAmazon().getAws().getSigningRegion();
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard()
            .withCredentials(new AWSStaticCredentialsProvider(amazonAWSCredentials()));
        builder.setEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, signingRegion));
        return builder.build();
    }

    @Bean
    public AWSCredentials amazonAWSCredentials() {
        String accessKey = applicationProperties.getAmazon().getAws().getAccessKey();
        String secretKey = applicationProperties.getAmazon().getAws().getSecretKey();
        return new BasicAWSCredentials(accessKey, secretKey);
    }


    @Bean
    public Dynamobee mongobee(AmazonDynamoDB db) {
        Dynamobee mongobee = new Dynamobee(db);
        // package to scan for migrations
        mongobee.setChangeLogsScanPackage("com.slalom.tchainz.config.dbmigrations");
        mongobee.setEnabled(true);
        return mongobee;
    }
}
