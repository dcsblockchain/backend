package com.slalom.tchainz.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Tchainz.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private final Amazon amazon = new Amazon();

    public final Amazon getAmazon() {
        return amazon;
    }

    public static class Amazon {

        private final Aws aws = new Aws();

        private final DynamoDb dynamoDb = new DynamoDb();

        public Aws getAws() {
            return aws;
        }

        public DynamoDb getDynamoDb() {
            return dynamoDb;
        }

        public static class Aws {

            private String accessKey;

            private String secretKey;

            private String signingRegion;

            public String getAccessKey() {
                return accessKey;
            }

            public void setAccessKey(String accessKey) {
                this.accessKey = accessKey;
            }

            public String getSecretKey() {
                return secretKey;
            }

            public void setSecretKey(String secretKey) {
                this.secretKey = secretKey;
            }

            public String getSigningRegion() {
                return signingRegion;
            }

            public void setSigningRegion(String signingRegion) {
                this.signingRegion = signingRegion;
            }
        }

        public static class DynamoDb {

            private String endPoint;

            public String getEndPoint() {
                return endPoint;
            }

            public void setEndPoint(String endPoint) {
                this.endPoint = endPoint;
            }
        }
    }
}
