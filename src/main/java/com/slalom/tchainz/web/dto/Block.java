package com.slalom.tchainz.web.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.codec.digest.DigestUtils;

import static org.apache.commons.codec.digest.MessageDigestAlgorithms.SHA_256;

public class Block {
    private long index;
    private String hash;
    private String previousHash;
    private TitleTransferData data;
    private long timeStamp;
    private String nonce;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public TitleTransferData getData() {
        return data;
    }

    public void setData(TitleTransferData data) {
        this.data = data;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public long getIndex() {
        return index;
    }

    public void setIndex(long index) {
        this.index = index;
    }

    @JsonIgnore
    public boolean isValid() {
        DigestUtils md = new DigestUtils(SHA_256);
            String hash = md.digestAsHex(
                getPreviousHash() +
                    Long.toString(getIndex()) +
                    getData() +
                    Long.toString(getTimeStamp()) +
                    getNonce());
            return hash.startsWith("00") && hash.equals(getHash());
    }
}
