/**
 * View Models used by Spring MVC REST controllers.
 */
package com.slalom.tchainz.web.rest.vm;
